import React, { useReducer } from 'react';

import uuid from 'uuid';

import proyectoContext from './proyectoContext';
import proyectoReducer from './proyectoReducer';
import {
	FORMULARIO_PROYECTO,
	OBTENER_PROYECTOS,
	AGREGAR_PROYECTO,
	VALIDAR_FORMULARIO,
} from '../../types'; //como el archivo se llama index no lo ponemos

const ProyectoState = (props) => {
	const proyectos = [
		{ id: 1, nombre: 'Tienda Virtual' },
		{ id: 2, nombre: 'Intranet' },
		{ id: 3, nombre: 'tercero' },
		{ id: 4, nombre: 'proyecto State' },
	];

	const initialState = {
		proyectos: [],
		formulario: false,
		errorformulario: false,
	};

	//dispatch para ejecutar acciones
	const [state, dispatch] = useReducer(proyectoReducer, initialState);

	//serie de instrucciones para CRUD
	const mostrarFormulario = () => {
		//cuando se llame a la funcion ejecutamos el dispatch--type
		dispatch({
			type: FORMULARIO_PROYECTO,
		});
	};

	//otener los proyectos
	const obtenerProyectos = () => {
		dispatch({
			type: OBTENER_PROYECTOS,
			payload: proyectos,
		});
	};

	//agregar nuevo proyecto
	const agregarProyecto = (proyecto) => {
		proyecto.id = uuid.v4();

		//inserta el proyecto en el state con el dispatch
		dispatch({
			type: AGREGAR_PROYECTO,
			payload: proyecto,
		});
	};

	//Validar formulario por errores
	const mostrarError = () => {
		dispatch({
			type: VALIDAR_FORMULARIO,
		});
	};

	return (
		<proyectoContext.Provider
			value={{
				proyectos: state.proyectos,
				formulario: state.formulario,
				errorformulario: state.errorformulario,
				mostrarFormulario,
				obtenerProyectos,
				agregarProyecto,
				mostrarError,
			}}
		>
			{props.children}
		</proyectoContext.Provider>
	);
};

export default ProyectoState;
