import React, { useState } from 'react';

import { Link } from 'react-router-dom';

const NuevaCuenta = () => {
	//State para iniciar sesio
	const [usuario, guardarUsuario] = useState({
        email: '',
        nombre:'',
        password: '',
        confirmar:''
	});

	//destructuracion de la variable usuario
	const { email, nombre, password, confirmar } = usuario;

	const onChange = (e) => {
		guardarUsuario({
			...usuario, //guardamos una copia de usuario
			[e.target.name]: e.target.value, //copiamos y dejamos el nuevo valor del formulario
		});
	};

	//Cuando el usuario quiere iniciar sesion
	const onSubmit = (e) => {
		e.preventDefault(); //hacer esta accion hasta que se precione el boton

		// validar que no haya campos vacios

        //password minimo de 6 caracteres

        //los dos password sean iguales

		//pasarlo al action
	};

	return (
		<div className="form-usuario">
			<div className="contenedor-form sombra-dark">
				<h1>Obtener Cuenta</h1>

				<form onSubmit={onSubmit}>
					<div className="campo-form">
						<label htmlFor="email">Email</label>
						<input
							type="email"
							id="email"
							name="email"
							placeholder="Escribe tu email:"
							value={email}
							onChange={onChange}
						/>
					</div>

					<div className="campo-form">
						<label htmlFor="nombre">Nombre del usuario</label>
						<input
							type="text"
							id="nombre"
							name="nombre"
							placeholder="Escribe tu nombre:"
							value={nombre}
							onChange={onChange}
						/>
					</div>

					<div className="campo-form">
						<label htmlFor="password">Password</label>
						<input
							type="password"
							id="password"
							name="password"
							placeholder="Escribe tu password:"
							value={password}
							onChange={onChange}
						/>
					</div>

					<div className="campo-form">
						<label htmlFor="confirmar">Confirmar Password</label>
						<input
							type="password"
							id="confirmar"
							name="confirmar"
							placeholder="Confirma tu password:"
							value={confirmar}
							onChange={onChange}
						/>
					</div>

					<div className="campo-form">
						<input
							type="submit"
							className="btn btn-primario btn-block"
							value="Registrarme"
						/>
					</div>
				</form>
				<Link to={'/'} className="enlace-cuenta">
					Volver a Iniciar sesión
				</Link>
			</div>
		</div>
	);
};

export default NuevaCuenta;
