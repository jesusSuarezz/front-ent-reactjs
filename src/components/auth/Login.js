import React, {useState} from 'react';

import {Link} from 'react-router-dom';

const Login = () => {

	//State para iniciar sesio
	const [usuario, guardarUsuario] = useState({ 
		email: '',
		password:''
	 });

	 //destructuracion de la variable usuario
	 const {email, password} = usuario

    const onChange = (e) => {
        guardarUsuario({
			...usuario, //guardamos una copia de usuario
			[e.target.name] : e.target.value //copiamos y dejamos el nuevo valor del formulario
		})
	};
	

	//Cuando el usuario quiere iniciar sesion
	const onSubmit = e =>{
		e.preventDefault(); //hacer esta accion hasta que se precione el boton

		// validar que no haya campos vacios 

		//pasarlo al action
	}

    return (
			<div className="form-usuario">
				<div className="contenedor-form sombra-dark">
					<h1>Iniciar Sesión</h1>

					<form onSubmit={onSubmit} >
						<div className="campo-form">
							<label htmlFor="email">Email</label>
							<input
								type="email"
								id="email"
								name="email"
								placeholder="Escribe tu email:"
								value={email}
								onChange={onChange}
							/>
						</div>
						<div className="campo-form">
							<label htmlFor="password">Password</label>
							<input
								type="password"
								id="password"
								name="password"
								placeholder="Escribe tu password:"
								value={password}
								onChange={onChange}
							/>
						</div>
						<div className="campo-form">
							<input
								type="submit"
								className="btn btn-primario btn-block"
								value="Iniciar Sesión"
							/>
						</div>
					</form>
					<Link to={'/nueva-cuenta'} className="enlace-cuenta">
						Obtener cuenta
					</Link>
				</div>
			</div>
		);
}

export default Login;