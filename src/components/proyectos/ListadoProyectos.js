import React, { useContext, useEffect } from 'react';

import Proyecto from './Proyecto';
import proyectoContext from '../../context/proyectos/proyectoContext'

const ListadoProyectos = () => {

	//extraer proyectos de state inicial
	const proyectosContext = useContext(proyectoContext); //accedemos a todos los datos que contega nuestro state de context
	const { proyectos, obtenerProyectos } = proyectosContext;

	useEffect(() => {
		obtenerProyectos();
	}, []);

	//revisar si proyectos tiene contenido
	if(proyectos.length === 0 ) return null;

	return (
		<ul className="listado-proyectos">
			{proyectos.map((proyectos) => (
				<Proyecto 
				key={proyectos.id}
                    proyecto={proyectos}
                />
			))}
		</ul>
	);
};

export default ListadoProyectos;
